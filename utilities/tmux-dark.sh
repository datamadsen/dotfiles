#### COLOUR (Solarized dark)

# default statusbar colors
set-option -g status-bg black
set-option -g status-fg white 
set-option -g status-attr default

# default window title colors
set-window-option -g window-status-fg white
set-window-option -g window-status-bg black
set-window-option -g window-status-attr default

# active window title colors
set-window-option -g window-status-current-fg green
set-window-option -g window-status-current-bg default
set-window-option -g window-status-current-attr default

# pane border
set-option -g pane-border-fg black 
set-option -g pane-active-border-fg brightgreen

# message text
set-option -g message-bg black
set-option -g message-fg brightred

# pane number display
set-option -g display-panes-active-colour blue
set-option -g display-panes-colour brightred

# clock
set-window-option -g clock-mode-colour green
